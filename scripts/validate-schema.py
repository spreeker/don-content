#!/usr/bin/env python3
import argparse
import json
from pathlib import Path
import sys

from jsonschema import Validator
from jsonschema.cli import _Outputter, _PrettyFormatter
from jsonschema.exceptions import best_match
from jsonschema.validators import validator_for
import yaml


def load_schema(schema_path: Path) -> Validator:
    with schema_path.open() as f:
        schema = json.load(f)
        cls = validator_for(schema)
        cls.check_schema(schema)
        validator = cls(schema)

    return validator


def validate_file(file_path: Path, validator: Validator, outputter: _Outputter) -> bool:
    ok = True

    with file_path.open() as f:
        instance = yaml.safe_load(f)

    if (error := best_match(validator.iter_errors(instance))) is not None:
        ok = False
        outputter.validation_error(instance_path=file_path, error=error)

    return ok


def main(schema_path: Path, path: Path) -> int:
    validator = load_schema(schema_path)
    outputter = _Outputter(_PrettyFormatter(), stdout=sys.stdout, stderr=sys.stderr)

    if path.is_file():
        ok = validate_file(path, validator, outputter)
        return 1 if not ok else 0

    has_failures = False

    for file in sorted(path.glob("*.yaml")):
        if not validate_file(file, validator, outputter):
            has_failures = True

    if has_failures:
        return 1

    return 0


def parse_aruments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Validate files against a JSON Schema")

    parser.add_argument("schema", help="Path to a JSON Schema")
    parser.add_argument("directory", help="Directory to validate")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_aruments()
    exit(main(Path(args.schema), Path(args.directory)))
